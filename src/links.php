<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />

  <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700,800,900|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="style.css" type="text/css" />
  <link rel="stylesheet" href="styles/ms_style.css" type="text/css" />
  <link rel="stylesheet" href="css/dark.css" type="text/css" />
  <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

  <link rel="stylesheet" href="css/responsive.css" type="text/css" />

  <script src='https://www.google.com/recaptcha/api.js'></script>
  
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

  <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
  <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">


  <!-- Document Title
  ============================================= -->
  <title>Epic Martial Arts | Links</title>

  <style>

    .demos-filter {
      margin: 0;
      text-align: right;
    }

    .demos-filter li {
      list-style: none;
      margin: 10px 0px;
    }

    .demos-filter li a {
      display: block;
      border: 0;
      text-transform: uppercase;
      letter-spacing: 1px;
      color: #444;
    }

    .demos-filter li a:hover,
    .demos-filter li.activeFilter a { color: #1ABC9C; }

    @media (max-width: 991px) {
      .demos-filter { text-align: center; }

      .demos-filter li {
        float: left;
        width: 33.3%;
        padding: 0 20px;
      }
    }

    @media (max-width: 767px) {
      .demos-filter li { width: 50%; }
    }

    .revo-slider-emphasis-text {
      font-size: 64px;
      font-weight: 700;
      letter-spacing: -1px;
      font-family: 'Raleway', sans-serif;
      padding: 15px 20px;
      border-top: 2px solid #FFF;
      border-bottom: 2px solid #FFF;
    }

    .revo-slider-desc-text {
      font-size: 20px;
      font-family: 'Lato', sans-serif;
      width: 650px;
      text-align: center;
      line-height: 1.5;
    }

    .revo-slider-caps-text {
      font-size: 16px;
      font-weight: 400;
      letter-spacing: 3px;
      font-family: 'Raleway', sans-serif;
    }
    .tp-video-play-button { display: none !important; }

    .tp-caption { white-space: nowrap; }

  </style>

</head>

<body class="stretched">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

  <!-- Header
    ============================================= -->
    <header id="header" class="dark">

      <?php include("templates/navigation.html"); ?>

    </header><!-- #header end -->

    <!-- Slider
    ============================================= -->

    <section id="slider" class="revslider-wrap clearfix">

      <div id="rev_slider_108_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="food-carousel80" style="margin:0px auto;background-color:#eef0f1;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
        <div id="rev_slider_108_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
          <ul>
            <!-- SLIDE  -->
            <li data-index="rs-326" data-transition="fade" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="RS_STYLES_IMAGES/JUDO_100X150.png"  data-rotate="0"  data-saveperformance="off"  data-title="USA Judo" data-description="">
              <!-- MAIN IMAGE -->
              <img src="RS_STYLES_IMAGES/JUDO_600x800.png"  alt=""  data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption FoodCarousel-Content   tp-resizeme"
                 id="slide-326-layer-3"
                 data-x="center" data-hoffset=""
                 data-y="center" data-voffset=""
                      data-width="['420']"
                data-height="['420']"
                data-transform_idle="o:1;"

                 data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:800;e:Power3.easeInOut;"
                 data-transform_out="auto:auto;s:500;"
                data-start="bytrigger"
                data-splitin="none"
                data-splitout="none"
                data-responsive_offset="on"

                 data-end="bytrigger"
                data-lasttriggerstate="reset"
                style="z-index: 5; min-width: 420px; max-width: 420px; max-width: 420px; max-width: 420px; white-space: normal;"><span class="foodcarousel-headline">USA Judo</span><br/>

                <hr  style="border-top: 1px solid #292e31;" />
                  USA Judo is a world leader providing maximum opportunities to all its members with programs from core development to Olympic gold.<br />
                  <a href="http://www.teamusa.org/USA-Judo" target="_blank">Visit Website</a>
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption FoodCarousel-Button rev-btn "
                 id="slide-326-layer-1"
                 data-x="center" data-hoffset=""
                 data-y="bottom" data-voffset="50"
                      data-width="['auto']"
                data-height="['auto']"
                data-transform_idle="o:1;"
                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                  data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 1.00);cursor:pointer;"

                 data-transform_in="opacity:0;s:300;e:Power3.easeInOut;"
                 data-transform_out="opacity:0;s:300;s:300;"
                data-start="0"
                data-splitin="none"
                data-splitout="none"
                data-actions='[{"event":"click","action":"startlayer","layer":"slide-326-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-326-layer-5","delay":"200"},{"event":"click","action":"stoplayer","layer":"slide-326-layer-1","delay":""}]'
                data-responsive_offset="on"
                data-responsive="off"
                      data-lasttriggerstate="reset"
                style="z-index: 6; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color: #444;"><i class="icon-file-alt" style="font-size: 21px; float: left;margin-top:0px;margin-right:10px;"></i> MORE INFO
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption FoodCarousel-CloseButton rev-btn  tp-resizeme"
                 id="slide-326-layer-5"
                 data-x="441"
                 data-y="110"
                      data-width="['auto']"
                data-height="['auto']"
                data-transform_idle="o:1;"
                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                  data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 1.00);cursor:pointer;"

                 data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:800;e:Power3.easeInOut;"
                 data-transform_out="auto:auto;s:500;"
                data-start="bytrigger"
                data-splitin="none"
                data-splitout="none"
                data-actions='[{"event":"click","action":"stoplayer","layer":"slide-326-layer-3","delay":""},{"event":"click","action":"stoplayer","layer":"slide-326-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-326-layer-1","delay":""}]'
                data-responsive_offset="on"

                 data-end="bytrigger"
                data-lasttriggerstate="reset"
                style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color:#444"><i class="icon-remove"></i>
              </div>
            </li>
            <!-- SLIDE  -->
            <li data-index="rs-327" data-transition="fade" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="RS_STYLES_IMAGES/MACHADO_100X150.png"  data-rotate="0"  data-saveperformance="off"  data-title="RCJ Machado Jiu-Jitsu" data-description="">
              <!-- MAIN IMAGE -->
              <img src="RS_STYLES_IMAGES/MACHADO_600x800.png"  alt=""  data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption FoodCarousel-Content   tp-resizeme"
                 id="slide-327-layer-3"
                 data-x="center" data-hoffset=""
                 data-y="center" data-voffset=""
                      data-width="['420']"
                data-height="['420']"
                data-transform_idle="o:1;"

                 data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:800;e:Power3.easeInOut;"
                 data-transform_out="auto:auto;s:500;"
                data-start="bytrigger"
                data-splitin="none"
                data-splitout="none"
                data-responsive_offset="on"

                 data-end="bytrigger"
                data-lasttriggerstate="reset"
                style="z-index: 5; min-width: 420px; max-width: 420px; max-width: 420px; max-width: 420px; white-space: normal;"><span class="foodcarousel-headline">RCJ Machado Jiu-Jitsu</span><br/>

                  <hr  style="border-top: 1px solid #292e31;" />
                    RCJ Machado Jiu-Jitsu believes the practice of Martial Arts, specifically Brazilian Jiu-Jitsu, 
                    has the ultimate goal of providing students with a tool to improve themselves as individuals, 
                    and adopt a healthier and more balanced lifestyle. That was the first and most important lesson 
                    we learned, and that is the one we strive to pass on to our students for decades to come.
                    <br />
                    <a href="http://www.carlosmachado.net/" target="_blank">Visit Website</a>
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption FoodCarousel-Button rev-btn "
                 id="slide-327-layer-1"
                 data-x="center" data-hoffset=""
                 data-y="bottom" data-voffset="50"
                      data-width="['auto']"
                data-height="['auto']"
                data-transform_idle="o:1;"
                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                  data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 1.00);cursor:pointer;"

                 data-transform_in="opacity:0;s:300;e:Power3.easeInOut;"
                 data-transform_out="opacity:0;s:300;s:300;"
                data-start="0"
                data-splitin="none"
                data-splitout="none"
                data-actions='[{"event":"click","action":"startlayer","layer":"slide-327-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-327-layer-5","delay":"200"},{"event":"click","action":"stoplayer","layer":"slide-327-layer-1","delay":""}]'
                data-responsive_offset="on"
                data-responsive="off"
                      data-lasttriggerstate="reset"
                style="z-index: 6; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color: #444;"><i class="icon-file-alt" style="font-size: 21px; float: left;margin-top:0px;margin-right:10px;"></i> MORE INFO
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption FoodCarousel-CloseButton rev-btn  tp-resizeme"
                 id="slide-327-layer-5"
                 data-x="441"
                 data-y="110"
                      data-width="['auto']"
                data-height="['auto']"
                data-transform_idle="o:1;"
                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                  data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 1.00);cursor:pointer;"

                 data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:800;e:Power3.easeInOut;"
                 data-transform_out="auto:auto;s:500;"
                data-start="bytrigger"
                data-splitin="none"
                data-splitout="none"
                data-actions='[{"event":"click","action":"stoplayer","layer":"slide-327-layer-3","delay":""},{"event":"click","action":"stoplayer","layer":"slide-327-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-327-layer-1","delay":""}]'
                data-responsive_offset="on"

                 data-end="bytrigger"
                data-lasttriggerstate="reset"
                style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color:#444"><i class="icon-remove"></i>
              </div>
            </li>
            <!-- SLIDE  -->
            <li data-index="rs-328" data-transition="fade" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="RS_STYLES_IMAGES/WORLDKIDO_100X150.png"  data-rotate="0"  data-saveperformance="off"  data-title="World Kido Federation / Haminjok Hapkido" data-description="">
              <!-- MAIN IMAGE -->
              <img src="RS_STYLES_IMAGES/WORLDKIDO_600x800.png"  alt=""  data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption FoodCarousel-Content   tp-resizeme"
                 id="slide-328-layer-3"
                 data-x="center" data-hoffset=""
                 data-y="center" data-voffset=""
                      data-width="['420']"
                data-height="['420']"
                data-transform_idle="o:1;"

                 data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:800;e:Power3.easeInOut;"
                 data-transform_out="auto:auto;s:500;"
                data-start="bytrigger"
                data-splitin="none"
                data-splitout="none"
                data-responsive_offset="on"

                 data-end="bytrigger"
                data-lasttriggerstate="reset"
                style="z-index: 5; min-width: 420px; max-width: 420px; max-width: 420px; max-width: 420px; white-space: normal;"><span class="foodcarousel-headline">World Kido Federation</span><br/>
                
                  <hr  style="border-top: 1px solid #292e31;" />
                    The World Kido Federation is one of the most respected and renowned Korean Martial Arts 
                    organizations in the world. We are led by a first generation martial artist, Grandmaster 
                    In Sun Seo, 10th Dan, whose martial arts history dates back to 1958.
                    <br />
                    <a href="http://worldkidofederation.com/" target="_blank">Visit Website</a>
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption FoodCarousel-Button rev-btn "
                 id="slide-328-layer-1"
                 data-x="center" data-hoffset=""
                 data-y="bottom" data-voffset="50"
                      data-width="['auto']"
                data-height="['auto']"
                data-transform_idle="o:1;"
                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                  data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 1.00);cursor:pointer;"

                 data-transform_in="opacity:0;s:300;e:Power3.easeInOut;"
                 data-transform_out="opacity:0;s:300;s:300;"
                data-start="0"
                data-splitin="none"
                data-splitout="none"
                data-actions='[{"event":"click","action":"startlayer","layer":"slide-328-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-328-layer-5","delay":"200"},{"event":"click","action":"stoplayer","layer":"slide-328-layer-1","delay":""}]'
                data-responsive_offset="on"
                data-responsive="off"
                      data-lasttriggerstate="reset"
                style="z-index: 6; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color: #444;"><i class="icon-file-alt" style="font-size: 21px; float: left;margin-top:0px;margin-right:10px;"></i> MORE INFO
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption FoodCarousel-CloseButton rev-btn  tp-resizeme"
                 id="slide-328-layer-5"
                 data-x="441"
                 data-y="110"
                      data-width="['auto']"
                data-height="['auto']"
                data-transform_idle="o:1;"
                  data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                  data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 1.00);cursor:pointer;"

                 data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:800;e:Power3.easeInOut;"
                 data-transform_out="auto:auto;s:500;"
                data-start="bytrigger"
                data-splitin="none"
                data-splitout="none"
                data-actions='[{"event":"click","action":"stoplayer","layer":"slide-328-layer-3","delay":""},{"event":"click","action":"stoplayer","layer":"slide-328-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-328-layer-1","delay":""}]'
                data-responsive_offset="on"

                data-end="bytrigger"
                data-lasttriggerstate="reset"
                style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color:#444"><i class="icon-remove"></i>
              </div>
            </li>
          </ul>
          <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div><!-- END REVOLUTION SLIDER -->
      </div>

    </section><!-- #content end -->


    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

      <?php include("templates/footer.html"); ?>

    </footer><!-- #footer end -->

  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/plugins.js"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script type="text/javascript" src="js/functions.js"></script>

  <!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
  <script type="text/javascript" src="include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

  <script type="text/javascript" src="include/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
  <script type="text/javascript" src="include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
  <script type="text/javascript" src="include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script type="text/javascript" src="include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
  <script type="text/javascript" src="include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>

  <script type="text/javascript">
    var tpj=jQuery;
    var revapi108;
    tpj(document).ready(function() {
      if(tpj("#rev_slider_108_1").revolution == undefined){
        revslider_showDoubleJqueryError("#rev_slider_108_1");
      }else{
        revapi108 = tpj("#rev_slider_108_1").show().revolution({
          sliderType:"carousel",
          jsFileLocation:"include/rs-plugin/js/",
          sliderLayout:"fullwidth",
          dottedOverlay:"none",
          delay:9000,
          navigation: {
            keyboardNavigation:"off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation:"off",
            onHoverStop:"off",
            arrows: {
              style:"metis",
              enable:true,
              hide_onmobile:true,
              hide_under:768,
              hide_onleave:false,
              tmp:'',
              left: {
                h_align:"left",
                v_align:"center",
                h_offset:0,
                v_offset:0
              },
              right: {
                h_align:"right",
                v_align:"center",
                h_offset:0,
                v_offset:0
              }
            }
            ,
            thumbnails: {
              style:"erinyen",
              enable:true,
              width:150,
              height:100,
              min_width:150,
              wrapper_padding:20,
              wrapper_color:"#000000",
              wrapper_opacity:"0.05",
              tmp:'<span class="tp-thumb-over"></span><span class="tp-thumb-image"></span><span class="tp-thumb-title">{{title}}</span><span class="tp-thumb-more"></span>',
              visibleAmount:9,
              hide_onmobile:false,
              hide_onleave:false,
              direction:"horizontal",
              span:true,
              position:"outer-bottom",
              space:10,
              h_align:"center",
              v_align:"bottom",
              h_offset:0,
              v_offset:0
            }
          },
          carousel: {
            maxRotation: 65,
            vary_rotation: "on",
            minScale: 55,
            vary_scale: "off",
            horizontal_align: "center",
            vertical_align: "center",
            fadeout: "on",
            vary_fade: "on",
            maxVisibleItems: 5,
            infinity: "on",
            space: -150,
            stretch: "off"
          },
          gridwidth:600,
          gridheight:600,
          lazyType:"none",
          shadow:0,
          spinner:"off",
          stopLoop:"on",
          stopAfterLoops:0,
          stopAtSlide:1,
          shuffle:"off",
          autoHeight:"off",
          disableProgressBar:"on",
          hideThumbsOnMobile:"off",
          hideSliderAtLimit:0,
          hideCaptionAtLimit:0,
          hideAllCaptionAtLilmit:0,
          debugMode:false,
          fallbacks: {
            simplifyAll:"off",
            nextSlideOnWindowFocus:"off",
            disableFocusListener:false,
          }
        });
      }
    });  /*ready*/
  </script>

</body>
</html>

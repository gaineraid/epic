<!DOCTYPE html>
<html dir="ltr" lang="en-US">
  <head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700,800,900|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Jockey+One" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/styles/schedule.css" type="text/css" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="styles/ms_style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="css/responsive.css" type="text/css" />

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
      <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->

    <!-- Document Title
    ============================================= -->
    <title>Epic Martial Arts | Schedule</title>



  </head>
  <body>

    <div class='container2'>
      <div style='margin-right: 15px;' class='item2'>
        <img src='images/LOGO_BW.png' width='275px' />
      </div>
            <table style="border: none; margin-left: 15px;" class='table item1'>
              <thead>
                <tr>
                  <th>Classes</th>
                  <th>Age Groups</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Lil' Dragons</td>
                  <td>4 - 6</td>
                </tr>
                <tr>
                  <td>Basic Training</td>
                  <td>7 - 11</td>
                </tr>
                <tr>
                  <td>Judo</td>
                  <td>7 - Adult</td>
                </tr>
                <tr>
                  <td>Kid's Hapkido</td>
                  <td>up - 12</td>
                </tr>
                <tr>
                  <td>Adult Hapkido</td>
                  <td>13 - up</td>
                </tr>
                <tr>
                  <td>Brazilian Jiu Jitsu</td>
                  <td>13 - Adult</td>
                </tr>

              </tbody>
            </table>
          </div>

          <br />

          <div class='container c3'>
            <table class='table item1'>
              <thead>
                <tr>
                  <th colSpan="2">Monday</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Ladies Kickboxing</td>
                  <td>1:30 - 2:30 p.m.</td>
                </tr>
                <tr>
                  <td>Lil' Dragons</td>
                  <td>4:00 - 4:30 p.m.</td>
                </tr>
                <tr>
                  <td>Judo</td>
                  <td>4:30 - 5:00 p.m.</td>
                </tr>
                <tr>
                  <td>Hapkido</td>
                  <td>5:00 - 5:50 p.m.</td>
                </tr>
                <tr>
                  <td>Basic Training</td>
                  <td>5:50 - 6:30 p.m.</td>
                </tr>
                <tr>
                  <td>BJJ Open Mat</td>
                  <td>6:30 - 7:30 p.m.</td>
                </tr>
              </tbody>
            </table>

            <table class='table item1'>
              <thead>
                <tr>
                  <th colSpan="2">Tuesday</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Basic Training</td>
                  <td>4:00 - 4:40 p.m.</td>
                </tr>
                <tr>
                  <td>Lil' Dragons</td>
                  <td>4:40 - 5:10 p.m.</td>
                </tr>
                <tr>
                  <td>Hapkido (white - purple belt)</td>
                  <td>5:10 - 5:50 p.m.</td>
                </tr>
                <tr>
                  <td>Hapkido (high purple and up)</td>
                  <td>5:50 - 6:30 p.m.</td>
                </tr>
                <tr>
                  <td>Knife and Stick</td>
                  <td>5:50 - 6:30 p.m.</td>
                </tr>
                <tr>
                  <td>Brazilian Jiu-Jitsu</td>
                  <td>6:30 - 7:30 p.m.</td>
                </tr>
              </tbody>
            </table>

            <table class='table item1'>
              <thead>
                <tr>
                  <th colSpan="2">Wednesday</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Private Lessons</td>
                  <td>By Appointment</td>
                </tr>
                <tr>
                  <td>Ladies Kickboxing</td>
                  <td>1:30 - 2:30 p.m.</td>
                </tr>
                <tr>
                  <td>Lil' Dragons</td>
                  <td>4:00 - 4:30 p.m.</td>
                </tr>
                <tr>
                  <td>Hapkido</td>
                  <td>4:40 - 5:30 p.m.</td>
                </tr>
                <tr>
                  <td>Basic Training</td>
                  <td>5:30 - 6:10 p.m.</td>
                </tr>
              </tbody>
            </table>

            <table class='table item1'>
              <thead>
                <tr>
                  <th colSpan="2">Thursday</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>BJJ</td>
                  <td>11:30 - 12:30 p.m.</td>
                </tr>
                <tr>
                  <td>Basic Training</td>
                  <td>4:00 - 4:40 p.m.</td>
                </tr>
                <tr>
                  <td>Judo</td>
                  <td>4:40 - 5:10 p.m.</td>
                </tr>
                <tr>
                  <td>Hapkido (white - purple belt)</td>
                  <td>5:10 - 5:50 p.m.</td>
                </tr>
                <tr>
                  <td>Hapkido (high purple and up)</td>
                  <td>5:50 - 6:30 p.m.</td>
                </tr>
                <tr>
                  <td>Knife and Stick</td>
                  <td>5:50 - 6:30 p.m.</td>
                </tr>
                <tr>
                  <td>Brazilian Jiu-Jitsu</td>
                  <td>6:30 - 8:00 p.m.</td>
                </tr>
              </tbody>
            </table>
          </div>

  </body>
</html>

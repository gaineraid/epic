<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />

  <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Jockey+One" rel="stylesheet">
  <link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="/style.css" type="text/css" />
  <link rel="stylesheet" href="/styles/ms_style.css" type="text/css" />
  <link rel="stylesheet" href="/css/swiper.css" type="text/css" />
  <link rel="stylesheet" href="/css/dark.css" type="text/css" />
  <link rel="stylesheet" href="/css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" />

  <link rel="stylesheet" href="/css/responsive.css" type="text/css" />

  <script src='https://www.google.com/recaptcha/api.js'></script>

  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

  <!-- Document Title
  ============================================= -->
  <title>Epic Martial Arts | About Us</title>

</head>

<body class="stretched">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="transparent-header dark">
      <?php include("templates/navigation.html"); ?>
    </header><!-- #header end -->

    <!-- Page Title
    ============================================= -->
    <section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('images/header_fix.png'); background-size: cover; background-position: center center;" data-stellar-background-ratio="0.4">

      <div style='margin-left: 35px;' class="container clearfix">
        <h1>About Us</h1>
        <span>Everything you need to know about Epic</span>
      </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">


        <div class="row common-height clearfix">

          <div class="col-sm-5 col-padding" style="background: url('images/chris.png') center center no-repeat; background-size: cover;"></div>

          <div class="col-sm-7 col-padding">
            <div>
              <div class="heading-block">
                <span class="jo-green">Founder &amp; Master Instructor</span>
                <h1>CHRIS HOLMES</h1>
              </div>

              <div class="row clearfix">

                <div class="col-md-6">
                  <p>
                    Master Chris Holmes has been studying various martial arts for over 22 years.  He started training in Tang Soo Do 
                    with Christian Shoemaker while attending USM in 1995.  Master Holmes then joined the Tylertown Tang Soo Do academy 
                    in 1997. Training became a way of life and he has consistently trained with the best instructors he could find 
                    since he first started in 1996. Master Holmes currently holds the following ranks: 5th Degree Black Belt in Hanminjok 
                    Hapkido under Grandmaster In Sun Seo, 5th Degree Black Belt in Jujitsu with the USJA, 3rd Degree Black Belt in 
                    Han Mu Do under Dr. He Young Kimm, 2nd Degree Black Belt in Tang Soo Do, 1st Degree Black Belt in Brazilian Jiu-Jitsu under 
                    Carlos Machado, and Black Belt in Judo. He is currently serving as Director for the World Kido Federation and 
                    Hanminjok Hapkido in the state of Mississippi and he is the official RCJ Machado Jiu-Jitsu Representative for Mississippi.
                  </p>
                  <p>
                    Master Holmes' first instructor Christian Shoemaker said this; "I've known Mr. Holmes since 1995. In that time, I 
                    have seen him grow tremendously as a martial artist. From a technical perspective, he is one of the best instructors 
                    I have worked with. His work ethic, desire to teach, and enthusiasm for seeing his students’ progress have always 
                    been phenomenal. It has been my honor and privilege to know him these last 20 years."
                  </p>
                </div>

              </div>

            </div>
          </div>

        </div>

        <div class="row common-height clearfix">

          <div class="col-sm-7 col-padding alt-color">
            <div>
              <div class="heading-block">
                <span class="jo-green">Instructor</span>
                <h1>CHRISTINE HOLMES</h1>
              </div>

              <div class="row clearfix">

                <div class="col-md-6">
                  <p>
                    Master Instructor Christine Holmes has been studying the martial arts for 15 years. She currently holds the ranks of 
                    4th Degree Black Belt in Hanminjok Hapkido under Grandmaster In Sun Seo, 1st Degree Black Belt in Han Mu Do under Dr. 
                    He Young Kimm, and Purple Belt in Brazilian Jiu-Jitsu under Carlos Machado and Professor Chris Holmes.
                  </p>
                  <p>
                    Mrs. Holmes has excelled at teaching all ages but her Lil' Dragons Program is truly remarkable because of the skills 
                    she is able to teach them. 
                  </p>
                </div>

              </div>

            </div>
          </div>

          <div class="col-md-5 col-padding" style="background: url('images/christine.png') center center no-repeat; background-size: cover;"></div>

        </div>

        <div class="row common-height clearfix">

          <div class="col-md-5 col-padding" style="background: url('images/little_dragons.jpg') center center no-repeat; background-size: cover;"></div>

          <div class="col-sm-7 col-padding">
            <div>
              <div class="heading-block">
                <span class="jo-green">4-6 year olds</span>
                <h1>LIL' DRAGONS</h1>
              </div>

              <div class="row clearfix">

                <div class="col-md-6">
                  <p>
                    This class is for 4-6yr olds, it teaches them the basics such as falling, kicks, punches, and a few throws. The 
                    focus of this class it to teach them basic martial art skills in addition to manners, teamwork, and life skills. 
                    They will be taught: home safety, stranger awareness, vacation safety, fire safety, traffic safety, weather safety, 
                    basic first aid, basic health concepts, and basic anti drug concepts.
                  </p>
                </div>

              </div>

            </div>
          </div>

        </div>

        <div class="row common-height clearfix">

          <div class="col-sm-7 col-padding alt-color">
            <div>
              <div class="heading-block">
                <span class="jo-green">7-12 year olds</span>
                <h1>BASIC TRAINING</h1>
              </div>

              <div class="row clearfix">

                <div class="col-sm-6">
                  <p>
                    This class is for 7-12 yr olds, it teaches the fundamental requirements for Hapkido and Judo through orange belt. 
                    They will learn falls, kicks, strikes, throws, and some joint locks. They will also be taught some Verbal Judo, 
                    anti bullying concepts, stranger awareness, and other life skills. The focus in this class is getting them ready 
                    for Hapkido and Judo. They also are required to do community service to earn their next belts. This is invaluable 
                    in cultivating leadership skills that they will need in the future.
                  </p>
                </div>

              </div>

            </div>
          </div>

          <div class="col-md-5 col-padding" style="background: url('images/basic.jpg') center center no-repeat; background-size: cover;"></div>

        </div>

        <div class="row common-height clearfix">

          <div class="col-md-5 col-padding" style="background: url('images/hapkido.jpg') center center no-repeat; background-size: cover;"></div>

          <div class="col-sm-7 col-padding">
            <div>
              <div class="heading-block">
                <h1>HAPKIDO/JUDO</h1>
              </div>

              <div class="row clearfix">

                <div class="col-md-6">
                  <p>
                    This is our core class for kids and families. This class teaches our students the core requirements of Hanminjok 
                    Hapkido as taught to us by Master Steve Seo. They will master their kicks, punches, and core hoshinsul(self defense techniques). 
                    In addition they will learn the core throws of Judo from a self defense and sporting perspective. More in-depth 
                    community service projects are required for belt promotion in this class as it is grooming our future community 
                    leaders.
                  </p>
                </div>

              </div>

            </div>
          </div>

        </div>

        <div class="row common-height clearfix">

          <div class="col-sm-7 col-padding alt-color">
            <div>
              <div class="heading-block">
                <h1>BRAZILIAN JIU-JITSU/JUDO</h1>
              </div>

              <div class="row clearfix">

                <div class="col-sm-6">
                  <p>
                    The BJJ/Judo classes are designed to teach our students ground fighting as taught to us by Grandmaster Carlos 
                    Machado and his brothers. We focus on creating an ego free environment where adults and teens can test themselves 
                    and grow while working as a team. Our students that decide to compete do very well in the tournament circuit. 
                    Our judo program supplements and enhances our BJJ.
                  </p>
                </div>

              </div>

            </div>
          </div>

          <div class="col-md-5 col-padding" style="background: url('images/carlos_choking_chris.jpg') center center no-repeat; background-size: cover;"></div>

        </div>


        <div class="row common-height clearfix">

          <div class="col-md-5 col-padding" style="background: url('images/hapkido.png') center center no-repeat; background-size: cover;"></div>

          <div class="col-sm-7 col-padding">
            <div>
              <div class="heading-block">
                <h1>JUDO</h1>
              </div>

              <div class="row clearfix">

                <div class="col-md-6">
                  <p>
                    Judo means the gentle way.  It was developed in Japan by Jigoro Kano around 1882. O Sensei Kano took techniques from 
                    multiple jujitsu systems and combined them into his own system which he called Judo. The two guiding principles of 
                    Judo are  maximum efficiency with minimum effort and mutual welfare and benefit. He eliminated the techniques of 
                    jujitsu that were the most dangerous and this allowed his students to train at a much higher level in relative safety. 
                    Here at Epic our Judo team is our competition team. Hapkido tournaments are almost impossible to find and Jiu Jitsu 
                    tournaments are not always safe for younger students. The competition Judo team allows them to tap into their competitive 
                    nature in a reasonably safe environment.
                  </p>
                </div>

              </div>

            </div>
          </div>

        </div>


    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

      <?php include("templates/footer.html"); ?>

    </footer><!-- #footer end -->

  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/plugins.js"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script type="text/javascript" src="js/functions.js"></script>

</body>
</html>

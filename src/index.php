<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />

  <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Jockey+One" rel="stylesheet">
  <link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="/style.css" type="text/css" />
  <link rel="stylesheet" href="/styles/ms_style.css" type="text/css" />
  <link rel="stylesheet" href="/css/swiper.css" type="text/css" />
  <link rel="stylesheet" href="/css/dark.css" type="text/css" />
  <link rel="stylesheet" href="/css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" />

  <link rel="stylesheet" href="/css/responsive.css" type="text/css" />

  <script src='https://www.google.com/recaptcha/api.js'></script>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

  <!-- Document Title
  ============================================= -->
  <title>Epic Martial Arts | Home </title>

</head>

<body class="stretched">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="transparent-header dark">

      <?php include("templates/navigation.html"); ?>

    </header><!-- #header end -->

    <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
      <div class="slider-parallax-inner">

        <div class="swiper-container swiper-parent">
          <div class="swiper-wrapper">

            <div class="swiper-slide dark" style="background-image: url('images/chalk_gi_compress.jpg');">
              <div class="container clearfix">
                <div class="slider-caption slider-caption-center quote_text">
                  <p id='quote' data-caption-animate="fadeInUp" data-caption-delay="200">WHERE THERE IS</p>
                  <h2 id='quote-mid' data-caption-animate="fadeInUp">PREPARATION</h2>
                  <p id='quote' data-caption-animate="fadeInUp" data-caption-delay="200">THERE IS NO FEAR</p>
                </div>
              </div>
            </div>
          </div>

        </div>

        <a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

      </div>
    </section>

    <!-- Content
    ============================================= -->
    <section id="content">

      <div style="padding-bottom: 0px !important;" class="content-wrap">

        <div class="container clearfix">
          <div class="row clearfix">

            <div class="col-lg-5">
              <div class="heading-block topmargin">
                <h1>WELCOME TO EPIC</h1>
              </div>
              <p class="lead">
                We are the premier martial arts destination in Southwest Mississippi. 
                We offer programs to get you in shape, build your confidence, to allow 
                you to deal with bullies, to teach you realistic and proven self defense, 
                and they are all taught in a professional family friendly facility. Come 
                in and see why families from as far as Jackson, Bassfield, and Monticello 
                come train with us.
              </p>
            </div>

            <div class="col-lg-7">
                <img src="images/LOGO_BW.png" alt="Chrome">
            </div>

          </div>
        </div>
        <br />

        <div class="belt_section">

          <div class="row topmargin-lg">

            <div style="text-align: center;" > 
              <div><h2 style="color: #fff; border-bottom: solid #81b441; display: inline-block;">WHY TRAIN EPIC MARTIAL ARTS?</h2></div>
            </div>

            <div class="col-md-4 col-sm-6 bottommargin">

              <div class="feature-box fbox-right topmargin" data-animate="fadeIn">
                <div class="fbox-icon belt-icon">
                  <a href="#"><i class="icon-line-check"></i></a>
                </div>
                <h3>FITNESS</h3>
                <p class="belt_left"> 
                    Martial arts offer many benefits, 
                    but when it comes to fitness, becoming 
                    a true martial artist means becoming a 
                    supremely fit person.
                </p>
              </div>

              <div class="feature-box fbox-right topmargin" data-animate="fadeIn" data-delay="50">
                <div class="fbox-icon belt-icon">
                  <a href="#"><i class="icon-line-check"></i></a>
                </div>
                <h3>FOCUS</h3>
                <p class="belt_left">
                  The greatest obstacle we face in this lifetime is ourselves. 
                  Martial arts teach us what it means to be still, challanged, and focused.
                </p>
              </div>

              <div class="feature-box fbox-right topmargin" data-animate="fadeIn" data-delay="100">
                <div class="fbox-icon belt-icon">
                  <a href="#"><i class="icon-line-check"></i></a>
                </div>
                <h3>PERSEVERENCE</h3>
                <p class="belt_left">
                  Part of life is learning how to take hits whether it be 
                  a literal blow or a dissappoinment from failing a test. 
                  Martial Arts teaches you how to effectively deal with these moments.
                </p>
              </div>

            </div>

            <div class="col-md-4 hidden-sm bottommargin center">
              <img style="align-self: center;" src="images/EPIC_WHITEBELT.png" alt="white belt">
            </div>

            <div class="col-md-4 col-sm-6 bottommargin">

              <div class="feature-box topmargin" data-animate="fadeIn">
                <div class="fbox-icon belt-icon">
                  <a href="#"><i class="icon-line-check"></i></a>
                </div>
                <h3>RESOLVE</h3>
                <p class="belt_right">
                  There are no fighting words. Martial arts teaches you 
                  to respond without reacting.
                </p>
              </div>

              <div class="feature-box topmargin" data-animate="fadeIn" data-delay="50">
                <div class="fbox-icon belt-icon">
                  <a href="#"><i class="icon-line-check"></i></a>
                </div>
                <h3>CONFIDENCE</h3>
                <p class="belt_right">
                  When you know in the back of your mind you have the skills to 
                  defend yourself, a sense of self-assuredness will surface.
                </p>
              </div>

              <div class="feature-box topmargin" data-animate="fadeIn" data-delay="100">
                <div class="fbox-icon belt-icon">
                  <a href="#"><i class="icon-line-check"></i></a>
                </div>
                <h3>SELF DEFENSE</h3>
                <p class="belt_right">
                  If the need ever arise you will have the confidence and skills 
                  to adequately defend yourself.
                </p>
              </div>

            </div>

          </div>

        </div>

        <div class="row clearfix common-height">

          <div class="col-md-6 center col-padding" style="background: url('images/micah.png') center center no-repeat; background-size: cover;">
            <div>&nbsp;</div>
          </div>

          <div class="col-md-6 center col-padding" style="background-color: #F5F5F5;">
            <div>
              <div class="heading-block nobottomborder">
                <div><h2 style="color: #81b441; display: inline-block;">Inside Epic</h2></div>
              </div>

              <div class="center bottommargin">
                <iframe id="ytplayer" type="text/html" width="640" height="360" src="https://www.youtube.com/embed/1WI6SJdwXFw" frameborder="0"></iframe>
              </div>
              <p class="lead nobottommargin">Come check us out and see for youself why we are the Premier Martial Arts destination in Southwest Mississippi</p>
            </div>
          </div>

        </div>


        <div id='parallax' class="section parallax dark nobottommargin" style="background-image: url('images/2.png'); padding: 100px 0;" data-stellar-background-ratio="0.4">

          <div class="heading-block center">
            <h3>WHAT PEOPLE SAY?</h3>
          </div>

          <div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
            <div class="flexslider">
              <div class="slider-wrap">
                <div class="slide">

                  <div class="testi-content">
                    <p>
                      Professor Holmes has top notch BJJ skills and is a great teacher. 
                      I would definitely recommend Epic Martial Arts Academy to anyone 
                      in the McComb area!!
                    </p>
                    <div class="testi-meta">
                      Kevin Bellard
                      <span>BJJ Black Belt</span>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

        </div>

      </div>


    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

      <?php include("templates/footer.html"); ?>

    </footer><!-- #footer end -->

  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/plugins.js"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script type="text/javascript" src="js/functions.js"></script>

</body>
</html>

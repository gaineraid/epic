<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />

  <!-- Stylesheets
  ============================================= -->
  <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Jockey+One" rel="stylesheet">
  <link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
  <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="/styles/style.css" type="text/css" />
  <link rel="stylesheet" href="/styles/ms_style.css" type="text/css" />
  <link rel="stylesheet" href="/css/dark.css" type="text/css" />
  <link rel="stylesheet" href="/css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" />

  <link rel="stylesheet" href="/css/responsive.css" type="text/css" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Document Title
  ============================================= -->
  <title>Epic Martial Arts | Events</title>

</head>

<body class="dark stretched">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="dark">
      <?php include($_SERVER['DOCUMENT_ROOT'] . "/templates/navigation.html"); ?>
    </header><!-- #header end -->

    <!-- Page Title
    ============================================= -->
    <section id="page-title">

      <div class="container clearfix">
        <h1>UPCOMING EVENTS</h1>
        <span>Don't miss out.</span>
        <!--
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Events</li>
        </ol>
        -->
      </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

      <div class="content-wrap">

        <div class="container clearfix">

          <div id="posts" class="events small-thumbs">

            <?php
              $args = array( 'posts_per_page' => -1, 'cat' => 9);
              $lastposts = get_posts( $args );
              foreach ( $lastposts as $post ) :
                setup_postdata( $post ); 
            ?>
              <div class="entry clearfix">
                <?php if( has_post_thumbnail( $post->ID ) ) { ?>
                  <div class="entry-image">
                    <img src='<?php the_post_thumbnail_url('medium'); ?>' />
                    <div id='date_overide' class="entry-date"><?php echo get_post_meta($post->ID, 'date', true); ?></div>
                  </div>
                <?php } ?>
                <div class="entry-c">
                  <div >
                    <h1 class="jockey"><?php the_title(); ?></h1>
                  </div>
                  <ul class="entry-meta clearfix">
                    <?php if(get_post_meta($post->ID, 'time', true)) { ?>
                      <li><i class="icon-time"></i> <?php echo get_post_meta($post->ID, 'time', true); ?></li>
                    <?php } ?>
                    <li><a href="http://maps.google.com/?q=<?php echo get_post_meta($post->ID, 'location', true); ?>"><i class="icon-map-marker2"></i> <?php echo get_post_meta($post->ID, 'location', true); ?></a></li>
                  </ul>
                  <div class="entry-content">
                    <p><?php the_content(); ?></p>
                    <!--<a href="#" class="btn btn-default" disabled="disabled">Buy Tickets</a> <a href="#" class="btn btn-danger">Read More</a>-->
                  </div>
                </div>
              </div>
            <?php
              endforeach;
              wp_reset_postdata();
            ?>




          </div>

          <!-- Pagination
          ============================================= 
          <ul class="pager nomargin">
            <li class="previous"><a href="#">&larr; Older</a></li>
            <li class="next"><a href="#">Newer &rarr;</a></li>
          </ul> .pager end -->

        </div>

      </div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">
      <?php include($_SERVER['DOCUMENT_ROOT'] . "/templates/footer.html"); ?>
    </footer><!-- #footer end -->

  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script type="text/javascript" src="/js/jquery.js"></script>
  <script type="text/javascript" src="/js/plugins.js"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script type="text/javascript" src="/js/functions.js"></script>

</body>
</html>

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'epic-wordpress');

/** MySQL database username */
define('DB_USER', 'epic');

/** MySQL database password */
define('DB_PASSWORD', 'nickel.109');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ix^XyCOOATs0=h}@<czok-gOzr+IC{yJ84K*jEI5hM{!j:G@&WFl<Qc{Zgoq,ctS');
define('SECURE_AUTH_KEY',  'sbEF)x+7z7W#O-)3K-Emhk<!m{s:qcnLsR*Ct4@<do!G.X0Oo0}WuL:+DHz02boA');
define('LOGGED_IN_KEY',    ':=uJK/4K,[C*izJ+BX/w>:NHR+XYyS,9IN+QodwQ@/yJfgEa&N077U[6`~DQ*UkJ');
define('NONCE_KEY',        ']2`:Xgkton</I>X5sx}6er{8-kCp#1b`a~*X`cKn~x[gGl_?E<ne{Keo{d|dhO!?');
define('AUTH_SALT',        '7:~S(+wzwYbos/tt)s@awaS=]g$d^X@{e/4:s/(i9ww%Wu765O[V#M@9wh-/NFAf');
define('SECURE_AUTH_SALT', 'RJdXPB0;~U;Cgwusz}7F3,v,)}xm!dy0hzTKyZv:4gp_DfAt`XlFNs&B 30;;,Mo');
define('LOGGED_IN_SALT',   'x}a-o}#~A*L.LI{rhEyDVE9(&WRS@)uvLI8VkM)BS)H[O*BpO-j{hVA3-A|vQp>t');
define('NONCE_SALT',       '5?y|_bcgz>g`ihOiR)YU1`2wenX0aIq<ly7fpw0aW7=+^l7_$Q}#&310)<#9V/{I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
